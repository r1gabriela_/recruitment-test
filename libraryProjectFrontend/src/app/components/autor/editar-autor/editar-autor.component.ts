import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Autor } from 'src/app/models/autor.model';
import { AutorService } from 'src/app/service/autor/autor.service';
import { CadastrarAutorComponent } from '../cadastrar-autor/cadastrar-autor.component';

@Component({
  selector: 'app-editar-autor',
  templateUrl: './editar-autor.component.html',
  styleUrls: ['./editar-autor.component.sass']
})
export class EditarAutorComponent implements OnInit {


  constructor(

    private autorService: AutorService,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router) {

      
    }

    autorForm: FormGroup;

  ngOnInit() {

    this.createForm()
  }

  createForm(){
    this.autorForm = this.fb.group({
      id: [''],
      nome: [null, Validators.required],
      nacionalidade: [null, Validators.required],
      dataNascimento: [ null, Validators.required]
    });

  }

  salvar(){
    let autor = this.autorForm.value
    this.route.paramMap.pipe(
      switchMap(params => {
        const id = +params.get('id');
        return this.autorService.editar(id, autor)
      })
    )
    .subscribe({
      next: (response) => {
        alert('Autor atualizado com sucesso!');
        this.router.navigate(['/autor/pesquisarAutor'])
      },
      error: (err) => {
        alert('Não foi possível atualizar o autor.')
      }
    })
  }


}
