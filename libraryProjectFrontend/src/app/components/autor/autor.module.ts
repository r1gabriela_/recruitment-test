import { CommonModule } from "@angular/common";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { AutorService } from "src/app/service/autor/autor.service";
import { AutorRoutingModule } from "./autor.routing.module";
import { CadastrarAutorComponent } from "./cadastrar-autor/cadastrar-autor.component";
import { EditarAutorComponent } from "./editar-autor/editar-autor.component";
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PesquisarAutorComponent } from "./pesquisar-autor/pesquisar-autor.component";




@NgModule({
    imports: [
      CommonModule,
      AutorRoutingModule,
      ReactiveFormsModule,
      Ng2SmartTableModule
    ],
    declarations: [
      CadastrarAutorComponent,
      EditarAutorComponent,
      PesquisarAutorComponent
    ],
    providers: [
      AutorService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
  })
  export class AutorModule { }