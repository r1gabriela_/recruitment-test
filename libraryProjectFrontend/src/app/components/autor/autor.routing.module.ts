import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CadastrarAutorComponent } from "./cadastrar-autor/cadastrar-autor.component";
import { EditarAutorComponent } from "./editar-autor/editar-autor.component";
import { PesquisarAutorComponent } from "./pesquisar-autor/pesquisar-autor.component";

const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Autor'
        },
        children : [
            {
                path: '',
                redirectTo: 'cadastrarAutor'
            },
            {
                path: 'cadastrarAutor',
                component: CadastrarAutorComponent,
                data: {
                    title: 'Cadastrar Autor'
                }
            },
            {
                path: 'editarAutor/:id',
                component: EditarAutorComponent,
                data: {
                    title: 'Editar Autor'
                }
            },
            {
                path: 'pesquisarAutor',
                component: PesquisarAutorComponent,
                data: {
                    title: 'Editar Autor'
                }
            },
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class AutorRoutingModule { }