import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Autor } from 'src/app/models/autor.model';
import { AutorService } from 'src/app/service/autor/autor.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-cadastrar-autor',
  templateUrl: './cadastrar-autor.component.html',
  styleUrls: ['./cadastrar-autor.component.sass'],
  providers: [ToastrService]
})
export class CadastrarAutorComponent implements OnInit {

  
  constructor(protected autorService: AutorService,
    protected fb: FormBuilder,
     protected toastr: ToastrService,
    ) { }

  autorForm: FormGroup;
  autor: Autor;
  autores: Autor[];
  formSubmitted: Boolean;
  isLoading: Boolean = false;

  ngOnInit() {
    this.createForm();
  }

  createForm(){
    this.autorForm = this.fb.group({
      nome: [null, Validators.required],
      nacionalidade: [null, Validators.required],
      dataNascimento: [ null, Validators.required]
    })
  }

  submit(){
    const autor = this.autorForm.value
    this.autorService.salvar(autor).pipe(finalize(() => this.isLoading = true)).subscribe(res => {
      alert('Autor cadastrado com sucesso')
    },(err) => {
      alert('Nao foi possível cadastrar o autor')
    })
  }



}
