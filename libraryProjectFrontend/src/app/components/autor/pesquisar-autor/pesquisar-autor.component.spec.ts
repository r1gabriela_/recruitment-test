import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesquisarAutorComponent } from './pesquisar-autor.component';

describe('PesquisarAutorComponent', () => {
  let component: PesquisarAutorComponent;
  let fixture: ComponentFixture<PesquisarAutorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesquisarAutorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesquisarAutorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
