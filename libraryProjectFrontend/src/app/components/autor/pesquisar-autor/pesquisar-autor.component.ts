import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServerDataSource } from 'ng2-smart-table';
import { AutorService } from 'src/app/service/autor/autor.service';

@Component({
  selector: 'app-pesquisar-autor',
  templateUrl: './pesquisar-autor.component.html',
  styleUrls: ['./pesquisar-autor.component.sass']
})
export class PesquisarAutorComponent implements OnInit {

  settings = {
    mode: 'external',
    attr: {
      class: 'table table-hover'
    },
    pager: {
      perPage: 10
    },
    actions: {
      columnTitle: 'Ações',
      position: 'right',
    },
    add: {
      addButtonContent: '<i class="fas fa-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="fas fa-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="fas fa-trash-alt"></i>',
    },
    columns: {
      id: {
        title: 'ID',
        width: '10%'
      },
      nome: {
        title: 'Nome',
      },
      nacionalidade: {
        title: 'Nacionalidade',
      },
      dataNascimento: {
        title: 'Data de Nascimento',
      },
    },
  };

  source: ServerDataSource;


  constructor(private router: Router,
    private autorService: AutorService) { }

  ngOnInit(){

    this.source = this.autorService.listar();
  }

  onCreate(event) {
    this.router.navigate(['autor/cadastrarAutor'])
  }

  onEdit(event) {
    const id = event.data.id;
    this.router.navigate(['autor/editarAutor/', id])
  }

  onDelete(event) {
    const id = event.data.id;
    console.log('DELETE:', id);
    this.autorService.delete(id).subscribe({
      next: (response) => {
        this.autorService.listar();
      }
    });
  }

}
