import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  constructor(protected fb: FormBuilder,
    protected router: Router) { }

  loginForm: FormGroup;
  isLoading : Boolean = false;


  ngOnInit() {
    this.createForm();
  }

  createForm(){
    this.loginForm = this.fb.group({
      login: [null, Validators.required],
      senha: [null, Validators.required]
    })
  }

  authenticate(){ 
    let user =  new User()
    if(this.loginForm.value.login ===  user.login && this.loginForm.value.senha == user.senha){
      this.router.navigate(['/livro/pesquisarLivro'])
      this.isLoading = true;
    }else {
      console.log(this.loginForm.value.login)
      console.log(this.loginForm.value.senha)
      alert('Credenciais Inválidas')
    }
    
    
  }


}
