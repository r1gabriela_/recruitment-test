import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServerDataSource } from 'ng2-smart-table';
import { LivroService } from 'src/app/service/livro/livro.service';

@Component({
  selector: 'app-pesquisar-livro',
  templateUrl: './pesquisar-livro.component.html',
  styleUrls: ['./pesquisar-livro.component.sass']
})
export class PesquisarLivroComponent implements OnInit {

  settings = {
    mode: 'external',
    attr: {
      class: 'table table-hover'
    },
    pager: {
      perPage: 10
    },
    actions: {
      columnTitle: 'Ações',
      position: 'right',
    },
    add: {
      addButtonContent: '<i class="fas fa-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="fas fa-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="fas fa-trash-alt"></i>',
    },
    columns: {
      id: {
        title: 'ID',
        width: '10%'
      },
      nome: {
        title: 'Nome',
      },
      nomeAutor: {
        title: 'Autor',
      },
      genero: {
        title: 'Gênero',
      },
      anoPublicacao: {
        title: 'Ano de Publicação',
      },
      editora: {
        title: 'Editora',
      },
      edicao: {
        title: 'Edição',
      },
      dataRecebimento: {
        title: 'Data Recebimento',
      },
     
    },
  };

  source: ServerDataSource;


  constructor(private router: Router,
    private livroService: LivroService) { }

  ngOnInit() {
    this.source = this.livroService.listar();
  }

  onCreate(event) {
    this.router.navigate(['livro/cadastrarLivro'])
  }

  onEdit(event) {
    const id = event.data.id;
    this.router.navigate(['livro/editarLivro/', id])
  }

  onDelete(event) {
    const id = event.data.id;
    console.log('DELETE:', id);
    this.livroService.delete(id).subscribe({
      next: (response) => {
        this.livroService.listar();
      }
    });
  }

}
