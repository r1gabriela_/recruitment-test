import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { switchMap } from 'rxjs/operators';
import { LivroService } from 'src/app/service/livro/livro.service';

@Component({
  selector: 'app-editar-livro',
  templateUrl: './editar-livro.component.html',
  styleUrls: ['./editar-livro.component.sass']
})
export class EditarLivroComponent implements OnInit {

  constructor(

    private livroService: LivroService,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router) {

      
    }

    livroForm: FormGroup;

  ngOnInit() {

    this.createForm()
  }

  createForm(){
    this.livroForm = this.fb.group({
      id: [''],
      nome: [null, Validators.required],
      nomeAutor: [null, Validators.required],
      genero: [ null, Validators.required],
      dataPublicacao: [ null, Validators.required],
      editora: [ null, Validators.required],
      edicao: [ null, Validators.required],
      dataRecebimento: [ null, Validators.required],
    });

  }

  salvar(){
    let livro = this.livroForm.value
    this.route.paramMap.pipe(
      switchMap(params => {
        const id = +params.get('id');
        return this.livroService.editar(id, livro)
      })
    )
    .subscribe({
      next: (response) => {
       alert('Livro atualizado com sucesso!');
        this.router.navigate(['/livro/pesquisarLivro'])
      },
      error: (err) => {
        alert('Não foi possível atualizar o Livro.')
      }
    })
  }

}
