import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Livro } from 'src/app/models/livro.model';
import { LivroService } from 'src/app/service/livro/livro.service';

@Component({
  selector: 'app-cadastrar-livro',
  templateUrl: './cadastrar-livro.component.html',
  styleUrls: ['./cadastrar-livro.component.sass']
})
export class CadastrarLivroComponent implements OnInit {

  constructor(private livroService: LivroService, 
    private fb: FormBuilder,
    private toastr: ToastrService) { }

    livroForm: FormGroup;
    selectedLivro: Livro;
    livro: Livro;
    livros: Livro[];
    newLivro: Boolean;
    formSubmitted: Boolean;

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.livroForm = this.fb.group({
      nome: [null, Validators.required],
      nomeAutor: [null, Validators.required],
      dataPublicacao: [null, Validators.required],
      genero: [ null, Validators.required],
      editora: [null, Validators.required],
      edicao: [null, Validators.required]
    })
  }


  submit(){
    const livro = this.livroForm.value
    this.livroService.salvar(livro).subscribe(res => {
      alert('Livro cadastrado com sucesso')
    }, (err) => {
      alert('Nao foi possível cadastrar o livro')
    })
  }

}
