import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CadastrarLivroComponent } from "./cadastrar-livro/cadastrar-livro.component";
import { EditarLivroComponent } from "./editar-livro/editar-livro.component";
import { PesquisarLivroComponent } from "./pesquisar-livro/pesquisar-livro.component";


const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Livro'
        },
        children : [
            {
                path: '',
                redirectTo: 'cadastrarLivro'
            },
            {
                path: 'cadastrarLivro',
                component: CadastrarLivroComponent,
                data: {
                    title: 'Cadastrar Livro'
                }
            },
            {
                path: 'editarLivro/:id',
                component: EditarLivroComponent,
                data: {
                    title: 'Editar Livro'
                }
            },
            {
                path: 'pesquisarLivro',
                component: PesquisarLivroComponent,
                data: {
                    title: 'Pesquisar Livro'
                }
            },
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class LivroRoutingModule { }