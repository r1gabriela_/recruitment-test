import { CommonModule } from "@angular/common";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { Ng2SmartTableModule } from "ng2-smart-table";
import { CadastrarLivroComponent } from "./cadastrar-livro/cadastrar-livro.component";
import { EditarLivroComponent } from "./editar-livro/editar-livro.component";
import { LivroRoutingModule } from "./livro.routing.module";
import { PesquisarLivroComponent } from "./pesquisar-livro/pesquisar-livro.component";




@NgModule({
    imports: [
      CommonModule,
      LivroRoutingModule,
      ReactiveFormsModule,
      Ng2SmartTableModule
    ],
    declarations: [
      CadastrarLivroComponent,
      EditarLivroComponent,
      PesquisarLivroComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
  })
  export class LivroModule { }