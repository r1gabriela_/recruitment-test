import { Autor } from "./autor.model";

export class Livro {
    id: number;
    nome: string;
    genero: string;
    dataPublicacao: string;
    editora: string;
    editcao: string;
    dataRecebimento: Date;
    nomeAutor: string;

}