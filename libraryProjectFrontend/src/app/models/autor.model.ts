export class Autor {
    id: number;
    nome: String;
    nacionalidade: String;
    dataNascimento: Date;
}