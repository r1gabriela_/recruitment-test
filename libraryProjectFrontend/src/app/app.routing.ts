import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login'
    }
  },
  {
    path: 'autor',
    loadChildren: () => import('./components/autor/autor.module').then(m => m.AutorModule)
  },
  {
    path: 'livro',
    loadChildren: () => import('./components/livro/livro.module').then(m => m.LivroModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
