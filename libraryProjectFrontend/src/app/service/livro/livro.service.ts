import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Livro } from 'src/app/models/livro.model';
import { Observable } from 'rxjs';
import { ServerDataSource } from 'ng2-smart-table';

@Injectable({
  providedIn: 'root'
})
export class LivroService {

  private baseUrl = 'http://localhost:3000/livros'



  constructor(private httpClient: HttpClient
    ) { }

    salvar(livro: Livro): Observable<Livro> {
      return this.httpClient.post<Livro>(this.baseUrl, livro);
    }

    listar(): ServerDataSource {
      return new ServerDataSource(this.httpClient, {endPoint: this.baseUrl});
    }

    editar(id: number, livro: Livro): Observable<Livro> {
      const url = `${this.baseUrl}/${id}`;
      return this.httpClient.put<Livro>(url, livro)
    }

    delete(id: number): Observable<any> {
      const url = `${this.baseUrl}/${id}`;
      return this.httpClient.delete<any>(url);
    }
}
