import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ServerDataSource } from 'ng2-smart-table';
import { Observable } from 'rxjs';
import { Autor } from 'src/app/models/autor.model';

@Injectable({
  providedIn: 'root'
})
export class AutorService {


  private baseUrl= 'http://localhost:3000/autores'


  constructor(private httpClient: HttpClient
    ) { }

    salvar(autor: Autor): Observable<Autor> {
      debugger
      return this.httpClient.post<Autor>(this.baseUrl, autor);
    }

    listar(): ServerDataSource {
      return new ServerDataSource(this.httpClient, {endPoint: this.baseUrl});
    }

    editar(id: number, autor:Autor): Observable<any>{
      const url = `${this.baseUrl}/${id}`;
      return this.httpClient.put<any>(url, autor)
    }

    delete(id: number): Observable<any> {
      const url = `${this.baseUrl}/${id}`;
      return this.httpClient.delete<any>(url);
    }

    findById(id: number): Observable<any> {
      const url = `${this.baseUrl}/${id}`;
      return this.httpClient.get<any>(url);
    }
}
