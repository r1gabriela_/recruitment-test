const Sequelize = require('sequelize');
const database = require('../db');

const Autor = database.define('autor', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false, 
        primaryKey: true
    },
    nome: {
        type: Sequelize.STRING
    },
    nacionalidade: {
        type: Sequelize.STRING
    },
    dataNascimento: {
        type: Sequelize.STRING
    }
});

module.exports = Autor;