const Sequelize = require('sequelize');
const sequelize = require('../db');
const database = require('../db');

const Livro = database.define('livro', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    nome: {
        type: Sequelize.STRING
    },
    genero: {
        type: Sequelize.STRING
    },
    dataPublicacao: {
        type: Sequelize.STRING
    },
    editora: {
        type: Sequelize.STRING
    },
    edicao: {
        type: Sequelize.STRING
    },
    dataRecebimento: {
        type: Sequelize.STRING
    },
    nomeAutor: {
        type: Sequelize.STRING
    },
})

module.exports = Livro;