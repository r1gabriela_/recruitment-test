const livroDao = require('../DAO/livroDao');
var livroController = {
    addLivro: addLivro,
    findLivros: findLivros,
    findLivroById: findLivroById,
    updateLivro: updateLivro,
    deleteById: deleteById
}

function addLivro(req, res) {
    let livro = req.body;
    livroDao.create(livro).
        then((data) => {
            res.send(data);
        })
        .catch((error) => {
            console.log(error);
        });
}

function findLivroById(req, res) {
    livroDao.findById(req.params.id).
        then((data) => {
            res.send(data);
        })
        .catch((error) => {
            console.log(error);
        });
}

function deleteById(req, res) {
    livroDao.deleteById(req.params.id).
        then((data) => {
            res.status(200).json({
                message: "Book deleted successfully",
                livro: data
            })
        })
        .catch((error) => {
            console.log(error);
        });
}

function updateLivro(req, res) {
    livroDao.updateLivro(req.body, req.params.id).
        then((data) => {
            res.status(200).json({
                message: "Livro updated successfully",
                livro: data
            })
        })
        .catch((error) => {
            console.log(error);
        });
}

function findLivros(req, res) {
    livroDao.findAll().
        then((data) => {
            res.send(data);
        })
        .catch((error) => {
            console.log(error);
        });
}

module.exports = livroController;