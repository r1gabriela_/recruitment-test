const autorDao = require('../DAO/autorDao');
var autorController = {
    addAutor: addAutor,
    findAutores: findAutores,
    findAutorById: findAutorById,
    updateAutor: updateAutor,
    deleteById: deleteById
}

function addAutor(req, res) {
    let autor = req.body;
    autorDao.create(autor).
        then((data) => {
            res.send(data);
        })
        .catch((error) => {
            console.log(error);
        });
}

function findAutorById(req, res) {
    autorDao.findById(req.params.id).
        then((data) => {
            res.send(data);
        })
        .catch((error) => {
            console.log(error);
        });
}

function deleteById(req, res) {
    autorDao.deleteById(req.params.id).
        then((data) => {
            res.status(200).json({
                message: "Autor deleted successfully",
                autor: data
            })
        })
        .catch((error) => {
            console.log(error);
        });
}

function updateAutor(req, res) {
    autorDao.updateAutor(req.body, req.params.id).
        then((data) => {
            res.status(200).json({
                message: "Autor updated successfully",
                autor: data
            })
        })
        .catch((error) => {
            console.log(error);
        });
}

function findAutores(req, res) {
    autorDao.findAll().
        then((data) => {
            res.send(data);
        })
        .catch((error) => {
            console.log(error);
        });
}

module.exports = autorController;