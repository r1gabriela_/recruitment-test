const Livro = require('../Model/livro');
var livroDao = {
    findAll : findAll,
    create: create, 
    findById: findById, 
    deleteById: deleteById, 
    updateLivro: updateLivro
}

function findAll() {
    return Livro.findAll();
}

function findById(id) {
    return Livro.findByPk(id);
}

function deleteById(id) {
    return Livro.destroy({ where: { id: id } });
}

function create(livro) {
    var newLivro = new Livro(livro);
    return newLivro.save();
}

function updateLivro(livro, id) {
    var updateLivro = {
        nome: livro.nome,
        genero: livro.genero,
        dataPublicacao: livro.dataPublicacao,
        editora: livro.editora,
        edicao: livro.edicao,
        dataRecebimento: livro.dataRecebimento,
        nomeAutor: livro.nomeAutor,
    };
    return Livro.update(updateLivro, { where: { id: id } });
}
module.exports = livroDao;