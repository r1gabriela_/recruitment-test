const Autor = require('../Model/autor');
var autorDao = {
    findAll : findAll,
    create: create, 
    findById: findById, 
    deleteById: deleteById, 
    updateAutor: updateAutor
}

function findAll() {
    return Autor.findAll();
}

function findById(id) {
    return Autor.findByPk(id);
}

function deleteById(id) {
    return Autor.destroy({ where: { id: id } });
}

function create(autor) {
    var newAutor = new Autor(autor);
    return newAutor.save();
}

function updateAutor(autor, id) {
    var updateAutor = {
        nome: autor.nome,
        nacionalidade: autor.nacionalidade,
        dataNascimento: autor.dataNascimento,
    };
    return Autor.update(updateAutor, { where: { id: id } });
}
module.exports = autorDao;