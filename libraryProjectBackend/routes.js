const express = require('express');
const router = express.Router();
const livroRoutes = require('./Routes/livroRoute');
const autorRoutes = require('./Routes/autorRoute');

router.use('/livros', livroRoutes);
router.use('/autores', autorRoutes);

module.exports = router;
